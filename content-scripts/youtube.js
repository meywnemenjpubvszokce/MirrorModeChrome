var isEnabled;

console.log('- MirrorMode -> youtube.js loaded');

if (document.getElementById('mirror-mode') === null) {
    isEnabled = false;

    var icon = document.createElement('button');
    icon.setAttribute('id', 'mirror-mode');
    icon.setAttribute('class', 'ytp-button');
    icon.setAttribute('title', 'Mirror Mode');

    var iconImage = document.createElement('img');
    iconImage.setAttribute('src', chrome.runtime.getURL('icons/youtube/disabled.png'));
    iconImage.style.height = '100%';
    iconImage.style.width = '100%';
    iconImage.style.transform = 'scale(0.68)';

    icon.appendChild(iconImage);

    document.getElementsByClassName('ytp-right-controls')[0].insertBefore(icon, document.getElementsByClassName('ytp-fullscreen-button')[0]);

    chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
        console.log('- MirrorMode -> message got');
        if (request.state === 'clicked') {
            toggleMirrorVideo(icon, iconImage);
        }
    });

    document.getElementById('mirror-mode').addEventListener('click', () => {
        toggleMirrorVideo(icon, iconImage);
    });
} else {
    iconImage.setAttribute('src', chrome.runtime.getURL('icons/youtube/disabled.png'));
        
    chrome.runtime.sendMessage({
        'request': 'change-icon',
        'icon': 'disabled'
    });
}

function toggleMirrorVideo(icon, iconImage) {
    if (isEnabled) {
        document.getElementsByClassName('html5-video-container')[0].getElementsByTagName('video')[0].style.webkitTransform = '';

        iconImage.setAttribute('src', chrome.runtime.getURL('icons/youtube/disabled.png'));
        
        chrome.runtime.sendMessage({
            'request': 'change-icon',
            'icon': 'disabled'
        });

        isEnabled = false;
    } else {
        document.getElementsByClassName('html5-video-container')[0].getElementsByTagName('video')[0].style.webkitTransform = "rotateY(180deg)";

        iconImage.setAttribute('src', chrome.runtime.getURL('icons/youtube/enabled.png'));
        
        chrome.runtime.sendMessage({
            'request': 'change-icon',
            'icon': 'enabled'
        });

        isEnabled = true;
    }
}

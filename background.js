const ICON_PATH = {
    enabled: './icons/youtube/enabled.png',
    disabled: './icons/youtube/disabled.png'
};

chrome.browserAction.onClicked.addListener((tab) => {
    chrome.tabs.sendMessage(tab.id, {
                                state: 'clicked'
                            }
    );
    console.log('sent');
    console.log(tab.url);
    console.log(tab.id);
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.request === 'change-icon') {
        chrome.browserAction.setIcon({
            path: ICON_PATH[request.icon],
            tabId: sender.tab.id
        });
    }
});

chrome.webNavigation.onHistoryStateUpdated.addListener(() => {
    console.log('page changed');
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, (tabs) => {
        let tab = tabs[0];
        let url = tab.url;

        if (RegExp('^https://www.youtube.com/watch*').test(url) || RegExp('^https://www.youtube.com/embed/*').test(url)) {
            chrome.tabs.executeScript(null, {file: './content-scripts/youtube.js'});

            console.log('youtube.js injected');
        }
    });
});
